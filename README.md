# Space-Allocation

**Ein paar Gedanken zum Visual Formatting Model (HTML).**

Unvollständig.

Tools: Draw.io und Blender.

Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img
alt="Creative Commons Lizenzvertrag" style="border-width:0"
src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Dieses
Werk ist lizenziert unter einer <a rel="license"
href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons
Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International
Lizenz</a>.
